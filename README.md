# Anleitung zur Installation von Kiwi Browser, uBlock Origin, Buster, Tampermonkey und BETSLIX auf Android

## Schritt 1: Kiwi Browser installieren

1. Öffne den **Google Play Store** auf deinem Android-Gerät oder öffne den Link:
   [Kiwi Browser](https://play.google.com/store/apps/details?id=com.kiwibrowser.browser)
2. Suche nach **„Kiwi Browser“** in der Suchleiste.
3. Wähle **„Kiwi Browser“** (von „Geometry OU“) aus den Suchergebnissen.
4. Tippe auf **„Installieren“**, um den Browser auf dein Gerät herunterzuladen und zu installieren.

## Schritt 2: uBlock Origin installieren

1. Öffne den **Kiwi Browser**.
2. Gehe zur **Chrome Web Store Seite** von uBlock Origin:  
   [uBlock Origin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)
3. Klicke auf **„Hinzufügen“**, um uBlock Origin im Kiwi Browser zu installieren.
4. Bestätige die Installation, falls eine Aufforderung erscheint.

## Schritt 3: Buster: Captcha Solver for Humans installieren

1. Bleibe im **Kiwi Browser**.
2. Besuche die **Chrome Web Store Seite** von Buster:  
   [Buster: Captcha Solver for Humans](https://chrome.google.com/webstore/detail/buster-captcha-solver-for/mpbjkejclgfgadiemmefgebjfooflfhl)
3. Klicke auf **„Hinzufügen“**, um Buster im Kiwi Browser zu installieren.
4. Bestätige die Installation, falls eine Aufforderung erscheint.

## Schritt 4: Tampermonkey Legacy installieren

1. Gehe zur **Tampermonkey Webseite**:  
   [Tampermonkey Legacy Erweiterung](https://tampermonkey.net/?ext=dhdg&browser=chrome)
2. Klicke auf **„Installieren“** auf der Webseite. Dies fügt die Tampermonkey Legacy-Erweiterung deinem Kiwi Browser
   hinzu.
3. Bestätige die Installation, falls du dazu aufgefordert wirst.

## Schritt 5: BETSLIX Script von Greasyfork installieren

1. Öffne den **Kiwi Browser** und stelle sicher, dass Tampermonkey installiert ist.
2. Gehe zu **Greasyfork**:  
   [Greasyfork BETSLIX Script](https://greasyfork.org/en/scripts/429666-betslix-burning-series-enhancer)
3. Klicke auf **„Installieren“** auf der Seite des BETSLIX Scripts.
4. Tampermonkey wird das Script erkennen und dich auffordern, es zu installieren.
5. Bestätige die Installation, indem du auf **„Installieren“** klickst.

## Schritt 6: Überprüfen der Installation

1. Öffne **Tampermonkey** (klicke auf das Tampermonkey-Symbol im Kiwi Browser).
2. Gehe zur Registerkarte **„Meine Scripts“** und überprüfe, ob BETSLIX installiert und aktiviert ist.
3. Überprüfe **uBlock Origin** und **Buster** unter **„Erweiterungen“** im Kiwi Browser-Menü, um sicherzustellen, dass
   beide Erweiterungen aktiv sind.

## Schritt 7: Spenden nicht vergessen

[Paypal](https://www.paypal.com/donate/?hosted_button_id=H76K9EAMTW7X8)
---

Jetzt hast du **Kiwi Browser**, **uBlock Origin**, **Buster**, **Tampermonkey Legacy** und das **BETSLIX Script**
erfolgreich auf deinem Android-Gerät installiert!
